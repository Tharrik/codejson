﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeJSON
{
    public partial class JSON
    {

        public class Object : Data
        {
            public static JSON.Object New(string name)
            {
                return new JSON.Object(name);
            }

            internal List<Data> items;

            public int Size
            {
                get { return items.Count; }
            }

            internal Object(string name) : base(name, typeof(JSON.Object))
            {
                items = new List<Data>();
            }

            #region Indexer
            public Data GetItemByName(string name)
            {
                Data toReturn = null;

                int size = items.Count;
                for (int i = 0; i < size && toReturn != null; ++i)
                {
                    Data d = items[i];
                    if (name == d.Name)
                    {
                        toReturn = d;
                    }
                }

                return toReturn;
            }

            public Data this[string name]
            {
                get { return GetItemByName(name); }
            }

            public Data this[int index]
            {
                get { return items[index]; }
            }
            #endregion

            #region Data manipulation
            public void AddItem(Data item)
            {
                if (GetItemByName(item.Name) == null)
                {
                    items.Add(item);
                }
            }

            public void AddItems(params Data[] items)
            {
                int size = items.Length;

                for (int i = 0; i < size; ++i)
                {
                    AddItem(items[i]);
                }
            }

            public void Add()
            {
                AddItem(JSON.Value.New());
            }

            public void Add(string name)
            {
                AddItem(JSON.Value.New(name));
            }

            public void Add(string name, bool value)
            {
                AddItem(JSON.Value.New(name, value));
            }

            public void Add(string name, int value)
            {
                AddItem(JSON.Value.New(name, value));
            }

            public void Add(string name, long value)
            {
                AddItem(JSON.Value.New(name, value));
            }

            public void Add(string name, float value)
            {
                AddItem(JSON.Value.New(name, value));
            }

            public void Add(string name, double value)
            {
                AddItem(JSON.Value.New(name, value));
            }

            public void Add(string name, string value)
            {
                AddItem(JSON.Value.New(name, value));
            }

            public void Remove(string name)
            {
                int size = items.Count;
                bool pending = true;

                for (int i = 0; i < size && pending; ++i)
                {
                    if (items[i].Name == name) items.RemoveAt(i);
                }
            }

            public void ChangeName(string name, string newName)
            {
                if (GetItemByName(newName) != null) return;

                GetItemByName(name).Name = newName;
            }

            public void Clear()
            {
                items.Clear();
            }
            #endregion

            public override bool Equals(object obj)
            {
                JSON.Object other = obj as JSON.Object;

                if (other != null && base.Equals(other))
                {
                    int size = Size;

                    if (size != other.Size) return false;

                    for (int i = 0; i < size; ++i)
                    {
                        if (!items[i].Equals(other.items[i])) return false;
                    }
                }
                else return false;

                return true;
            }

            #region Serialization
            internal static JSON.Object Parse(string input, ref int index)
            {
                JSON.Object toReturn = JSON.Object.New("");

                int size = input.Length;
                bool pending = true;

                for(; pending && index < size; ++index)
                {
                    char current = input[index];

                    if (IsWhitespace(current)) continue;

                    if (current != '{') pending = false;
                    else toReturn.items = JSON.Object.ParseItems(input, ref index);
                }

                return toReturn;
            }

            internal static List<Data> ParseItems(string input,  ref int index)
            {
                List<Data> toReturn = new List<Data>();

                int size = input.Length;
                bool pending = true;
                string name = "";
                ParseState state = ParseState.Name;

                if (input[index] != '{') pending = false;
                else ++index;

                for(; pending && index < size; ++index)
                {
                    char current = input[index];

                    if (IsWhitespace(current)) continue;

                    if (current == '}')
                    {
                        pending = false;
                        continue;
                    }

                    switch(state)
                    {
                        case ParseState.Name:
                            if (current != '"') pending = false;
                            else
                            {
                                name = Data.ExtractString(input, ref index);
                                state = ParseState.Separator;
                            }
                            break;

                        case ParseState.Separator:
                            if (current != ':') pending = false;
                            else state = ParseState.Data;
                            break;

                        case ParseState.Data:
                            if (current == '{')
                            {
                                JSON.Object obj = JSON.Object.New(name);
                                obj.items = JSON.Object.ParseItems(input, ref index);

                                toReturn.Add(obj);
                            }
                            else if (current == '[')
                            {
                                JSON.Array array = JSON.Array.New(name);
                                array.items = JSON.Array.ParseItems(input, ref index);

                                toReturn.Add(array);
                            }
                            else
                            {
                                JSON.Value value = JSON.Value.Parse(input, ref index);
                                value.Name = name;

                                toReturn.Add(value);
                            }

                            state = ParseState.Next;
                            break;

                        case ParseState.Next:
                            if(current == ',') state = ParseState.Name;
                            else pending = false;
                            break;
                    }
                }

                --index;

                return toReturn;
            }

            public override string ToString()
            {
                return base.ToString() + ValueToString();
            }

            internal override string ValueToString()
            {
                string toReturn = "{ ";

                int size = Size;

                if (size > 0)
                {
                    toReturn += items[0].ToString();
                    for (int i = 1; i < size; ++i)
                    {
                        toReturn = string.Concat(toReturn, ", ", items[i].ToString());
                    }
                }

                toReturn += '}';

                return toReturn;
            }
            #endregion
        }

    }
}
