﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CodeJSON
{
    public partial class JSON
    {
        enum ParseState { Name, Separator, Value, Data, Next };

        public Object Get { get; internal set; }

        public int Size
        {
            get { return Get.Size; }
        }

        #region Constructors
        public JSON()
        {
            Get = JSON.Object.New("");
        }
        #endregion

        public override bool Equals(object obj)
        {
            JSON other = obj as JSON;

            if(other != null)
            {
                return JSON.Object.Equals(Get, other.Get);
            }
            else return false;
        }

        #region Serialization
        public static JSON FromJSON(string input)
        {
            JSON json = new JSON();
            int size = input.Length;

            for(int index = 0; index < size; ++index)
            {
                char current = input[index];

                if(current == '{') json.Get = JSON.Object.Parse(input, ref index);
            }

            return json;
        }

        public string ToJSON()
        {
            return Get.ValueToString();
        }

        public override string ToString()
        {
            return ToJSON();
        }
        #endregion

    }
}
