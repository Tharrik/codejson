﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CodeJSON
{
    public partial class JSON
    {
        public class Array : Data
        {
            #region Static Creators
            public static JSON.Array New(string name)
            {
                JSON.Array toReturn = new JSON.Array(name);

                return toReturn;
            }

            public static JSON.Array New(string name, bool value)
            {
                JSON.Array toReturn = new JSON.Array(name);

                toReturn.items.Add(JSON.Value.New("", value));

                return toReturn;
            }

            public static JSON.Array New(string name, int value)
            {
                JSON.Array toReturn = new JSON.Array(name);

                toReturn.items.Add(JSON.Value.New("", value));

                return toReturn;
            }

            public static JSON.Array New(string name, long value)
            {
                JSON.Array toReturn = new JSON.Array(name);

                toReturn.items.Add(JSON.Value.New("", value));

                return toReturn;
            }

            public static JSON.Array New(string name, float value)
            {
                JSON.Array toReturn = new JSON.Array(name);

                toReturn.items.Add(JSON.Value.New("", value));

                return toReturn;
            }

            public static JSON.Array New(string name, double value)
            {
                JSON.Array toReturn = new JSON.Array(name);

                toReturn.items.Add(JSON.Value.New("", value));

                return toReturn;
            }

            public static JSON.Array New(string name, string value)
            {
                JSON.Array toReturn = new JSON.Array(name);

                toReturn.items.Add(JSON.Value.New("", value));

                return toReturn;
            }

            public static JSON.Array New(string name, JSON.Array value)
            {
                JSON.Array toReturn = new JSON.Array(name);

                toReturn.items.Add(value);

                return toReturn;
            }

            public static JSON.Array New(string name, JSON.Object value)
            {
                JSON.Array toReturn = new JSON.Array(name);

                toReturn.items.Add(value);

                return toReturn;
            }

            public static JSON.Array New(string name, params bool[] values)
            {
                JSON.Array toReturn = new JSON.Array(name);

                int size = values.Length;
                toReturn.items.Capacity = size;

                for (int i = 0; i < size; i++)
                {
                    toReturn.items.Add(JSON.Value.New("", values[i]));
                }

                return toReturn;
            }

            public static JSON.Array New(string name, params int[] values)
            {
                JSON.Array toReturn = new JSON.Array(name);

                int size = values.Length;
                toReturn.items.Capacity = size;

                for (int i = 0; i < size; i++)
                {
                    toReturn.items.Add(JSON.Value.New("", values[i]));
                }

                return toReturn;
            }

            public static JSON.Array New(string name, params long[] values)
            {
                JSON.Array toReturn = new JSON.Array(name);

                int size = values.Length;
                toReturn.items.Capacity = size;

                for (int i = 0; i < size; i++)
                {
                    toReturn.items.Add(JSON.Value.New("", values[i]));
                }

                return toReturn;
            }

            public static JSON.Array New(string name, params float[] values)
            {
                JSON.Array toReturn = new JSON.Array(name);

                int size = values.Length;
                toReturn.items.Capacity = size;

                for (int i = 0; i < size; i++)
                {
                    toReturn.items.Add(JSON.Value.New("", values[i]));
                }

                return toReturn;
            }

            public static JSON.Array New(string name, params double[] values)
            {
                JSON.Array toReturn = new JSON.Array(name);

                int size = values.Length;
                toReturn.items.Capacity = size;

                for (int i = 0; i < size; i++)
                {
                    toReturn.items.Add(JSON.Value.New("", values[i]));
                }

                return toReturn;
            }

            public static JSON.Array New(string name, params string[] values)
            {
                JSON.Array toReturn = new JSON.Array(name);

                int size = values.Length;
                toReturn.items.Capacity = size;

                for (int i = 0; i < size; i++)
                {
                    toReturn.items.Add(JSON.Value.New("", values[i]));
                }

                return toReturn;
            }

            public static JSON.Array New(string name, params JSON.Array[] values)
            {
                JSON.Array toReturn = new JSON.Array(name);

                int size = values.Length;
                toReturn.items.Capacity = size;

                for (int i = 0; i < size; i++)
                {
                    toReturn.items.Add(values[i]);
                }

                return toReturn;
            }

            public static JSON.Array New(string name, params JSON.Object[] values)
            {
                JSON.Array toReturn = new JSON.Array(name);

                int size = values.Length;
                toReturn.items.Capacity = size;

                for (int i = 0; i < size; i++)
                {
                    toReturn.items.Add(values[i]);
                }

                return toReturn;
            }
            #endregion

            #region Fields
            internal List<Data> items;

            public int Size
            {
                get { return items.Count; }
            }
            #endregion

            #region Constructors
            internal Array(string name) : base(name, typeof(JSON.Array))
            {
                items = new List<Data>();
            }
            #endregion

            #region Indexer
            public Data GetAt(int index)
            {
                return this[index];
            }

            public Data this[int index]
            {
                get { return items[index]; }
                set { items[index] = value; }
            }
            #endregion

            #region Data Manipulation
            public void Add()
            {
                items.Add(JSON.Value.New(""));
            }

            public void Add(bool value)
            {
                items.Add(JSON.Value.New("", value));
            }

            public void Add(int value)
            {
                items.Add(JSON.Value.New("", value));
            }

            public void Add(long value)
            {
                items.Add(JSON.Value.New("", value));
            }

            public void Add(float value)
            {
                items.Add(JSON.Value.New("", value));
            }

            public void Add(double value)
            {
                items.Add(JSON.Value.New("", value));
            }

            public void Add(string value)
            {
                items.Add(JSON.Value.New("", value));
            }

            public void Add(JSON.Array value)
            {
                items.Add(value);
            }

            public void Add(JSON.Object value)
            {
                items.Add(value);
            }

            public void Add(params bool[] values)
            {
                int size = values.Length;
                items.Capacity += size;

                for (int i = 0; i < size; ++i)
                {
                    items.Add(JSON.Value.New("", values[i]));
                }
            }

            public void Add(params int[] values)
            {
                int size = values.Length;
                items.Capacity += size;

                for (int i = 0; i < size; ++i)
                {
                    items.Add(JSON.Value.New("", values[i]));
                }
            }

            public void Add(params long[] values)
            {
                int size = values.Length;
                items.Capacity += size;

                for (int i = 0; i < size; ++i)
                {
                    items.Add(JSON.Value.New("", values[i]));
                }
            }

            public void Add(params float[] values)
            {
                int size = values.Length;
                items.Capacity += size;

                for (int i = 0; i < size; ++i)
                {
                    items.Add(JSON.Value.New("", values[i]));
                }
            }

            public void Add(params double[] values)
            {
                int size = values.Length;
                items.Capacity += size;

                for (int i = 0; i < size; ++i)
                {
                    items.Add(JSON.Value.New("", values[i]));
                }
            }

            public void Add(params string[] values)
            {
                int size = values.Length;
                items.Capacity += size;

                for (int i = 0; i < size; ++i)
                {
                    items.Add(JSON.Value.New("", values[i]));
                }
            }

            public void Add(params JSON.Array[] values)
            {
                int size = values.Length;
                items.Capacity += size;

                for (int i = 0; i < size; ++i)
                {
                    items.Add(values[i]);
                }
            }

            public void Add(params JSON.Object[] values)
            {
                int size = values.Length;
                items.Capacity += size;

                for (int i = 0; i < size; ++i)
                {
                    items.Add(values[i]);
                }
            }

            public void Insert(int index)
            {
                items.Insert(index, JSON.Value.New());
            }

            public void Insert(int index, bool value)
            {
                items.Insert(index, JSON.Value.New("", value));
            }

            public void Insert(int index, int value)
            {
                items.Insert(index, JSON.Value.New("", value));
            }

            public void Insert(int index, long value)
            {
                items.Insert(index, JSON.Value.New("", value));
            }

            public void Insert(int index, float value)
            {
                items.Insert(index, JSON.Value.New("", value));
            }

            public void Insert(int index, double value)
            {
                items.Insert(index, JSON.Value.New("", value));
            }

            public void Insert(int index, string value)
            {
                items.Insert(index, JSON.Value.New("", value));
            }

            public void Insert(int index, JSON.Data item)
            {
                items.Insert(index, item);
            }

            public void Remove()
            {
                bool repeat = true;

                for (int i = 0; i < Size && repeat; ++i)
                {
                    if (items[i] is JSON.Value item && item.Type == null)
                    {
                        items.RemoveAt(i);
                        repeat = false;
                    }
                }
            }

            public void Remove(bool value)
            {
                bool repeat = true;

                for (int i = 0; i < Size && repeat; ++i)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Type == typeof(bool) && item.Get() == value)
                    {
                        items.RemoveAt(i);
                        repeat = false;
                    }
                }
            }

            public void Remove(int value)
            {
                bool repeat = true;

                for (int i = 0; i < Size && repeat; ++i)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Type == typeof(int) && item.Get() == value)
                    {
                        items.RemoveAt(i);
                        repeat = false;
                    }
                }
            }

            public void Remove(long value)
            {
                bool repeat = true;

                for (int i = 0; i < Size && repeat; ++i)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Type == typeof(long) && item.Get() == value)
                    {
                        items.RemoveAt(i);
                        repeat = false;
                    }
                }
            }

            public void Remove(float value)
            {
                bool repeat = true;

                for (int i = 0; i < Size && repeat; ++i)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Type == typeof(float) && item.Get() == value)
                    {
                        items.RemoveAt(i);
                        repeat = false;
                    }
                }
            }

            public void Remove(double value)
            {
                bool repeat = true;

                for (int i = 0; i < Size && repeat; ++i)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Type == typeof(double) && item.Get() == value)
                    {
                        items.RemoveAt(i);
                        repeat = false;
                    }
                }
            }

            public void Remove(string value)
            {
                bool repeat = true;

                for (int i = 0; i < Size && repeat; ++i)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Type == typeof(string) && item.Get() == value)
                    {
                        items.RemoveAt(i);
                        repeat = false;
                    }
                }
            }

            public void RemoveAll()
            {
                for (int i = 0; i < Size; i++)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Get() == null) items.RemoveAt(i--);
                }
            }

            public void RemoveAll(bool value)
            {
                for (int i = 0; i < Size; i++)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Get() == value) items.RemoveAt(i--);
                }
            }

            public void RemoveAll(int value)
            {
                for (int i = 0; i < Size; i++)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Get() == value) items.RemoveAt(i--);
                }
            }

            public void RemoveAll(long value)
            {
                for (int i = 0; i < Size; i++)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Get() == value) items.RemoveAt(i--);
                }
            }

            public void RemoveAll(float value)
            {
                for (int i = 0; i < Size; i++)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Get() == value) items.RemoveAt(i--);
                }
            }

            public void RemoveAll(double value)
            {
                for (int i = 0; i < Size; i++)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Get() == value) items.RemoveAt(i--);
                }
            }

            public void RemoveAll(string value)
            {
                for (int i = 0; i < Size; i++)
                {
                    JSON.Value item = items[i] as JSON.Value;
                    if (item != null && item.Get() == value) items.RemoveAt(i--);
                }
            }

            public void RemoveAt(int index)
            {
                items.RemoveAt(index);
            }

            public void Clear()
            {
                items.Clear();
            }
            #endregion

            public override bool Equals(object obj)
            {
                JSON.Array other = obj as JSON.Array;

                if (other != null && base.Equals(obj))
                {
                    if (Size != other.Size) return false;

                    for (int i = 0; i < Size; ++i)
                    {
                        if (!items[i].Equals(other.items[i])) return false;
                    }

                    return true;
                }

                return false;
            }

            #region Serialization
            internal static List<Data> ParseItems(string input, ref int index)
            {
                List<Data> toReturn = new List<Data>();

                int size = input.Length;
                bool pending = true;
                ParseState state = ParseState.Data;

                if (input[index] != '[') pending = false;
                else ++index;

                for ( ; pending && index < size ; ++index)
                {
                    char current = input[index];

                    if (IsWhitespace(current)) continue;

                    if (current == ']')
                    {
                        pending = false;
                        continue;
                    }

                    switch (state)
                    {
                        case ParseState.Data:
                            if (current == '{')
                            {
                                JSON.Object obj = JSON.Object.New("");
                                obj.items = JSON.Object.ParseItems(input, ref index);

                                toReturn.Add(obj);
                            }
                            else if (current == '[')
                            {
                                JSON.Array array = JSON.Array.New("");
                                array.items = JSON.Array.ParseItems(input, ref index);

                                toReturn.Add(array);
                            }
                            else
                            {
                                JSON.Value value = JSON.Value.Parse(input, ref index);

                                toReturn.Add(value);
                            }

                            state = ParseState.Next;
                            break;

                        case ParseState.Next:
                            if (current == ',') state = ParseState.Data;
                            else pending = false;
                            break;
                    }
                }

                --index;

                return toReturn;
            }

            public override string ToString()
            {
                return string.Concat("\"", Name, "\" : ", ValueToString());
            }

            internal override string ValueToString()
            {
                string toReturn = "[ ";

                int size = items.Count;

                if (size > 0)
                {
                    toReturn = string.Concat('[', items[0].ValueToString());
                    for (int i = 1; i < size; ++i)
                    {
                        toReturn = string.Concat(toReturn, ", ", items[i].ValueToString());
                    }
                }

                return string.Concat(toReturn, ']');
            }
            #endregion
        }

    }
}
