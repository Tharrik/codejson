﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CodeJSON
{
    // TODO
    public class JSONFile
    {
        string path = "";
        byte[] data = new byte[0];

        public bool IsValid { get; private set; }

        public JSONFile(string path)
        {
            this.path = path;

            IsValid = File.Exists(path);

            if (!IsValid) return;

            ReadFile();
        }

        public static implicit operator string(JSONFile file)
        {
            return Encoding.Unicode.GetString(file.data);
        }

        public void Write(byte[] data)
        {
            this.data = this.data.Concat(data).ToArray<byte>();
        }

        public void WriteAt(byte[] data, int begin, int end)
        {
            int writeSize = end - begin;

            // If needed resize data
            if (end >= this.data.Length)
            {
                byte[] newData = new byte[end + 1];
                int size = newData.Length;
                // Copy data to new array
                for (int i = 0; i < size; ++i)
                {
                    if (i < begin)
                    {
                        newData[i] = this.data[i];
                    }
                    else
                    {
                        newData[i] = data[i - begin];
                    }
                }

                this.data = newData;
            }
            else
            {
                for (int i = begin; i < end; ++i)
                {
                    this.data[i] = data[i - begin];
                }
            }
        }

        public void DeleteAt(int begin, int end)
        {
            if (begin <= 0 || begin >= end) return;

            int oldSize = data.Length;
            int newSize = data.Length - (end - begin);
            byte[] newData = new byte[newSize];

            // i is a counter for the old data
            // j is a counter for the new data
            for (int i = 0, j = 0; i < oldSize && j < newSize; ++i)
            {
                // Write to new data only if i is outside the delete range
                if (i < begin || i >= end)
                {
                    newData[j] = data[i];
                    ++j;
                }
            }

            data = newData;
        }

        public void Consolidate()
        {
            WriteFile();
        }

        #region Internal Methods
        protected void ReadFile()
        {
            FileStream file = File.Open(path, FileMode.Open);

            data = new byte[file.Length];

            file.Read(data);

            file.Close();
        }

        protected void WriteFile()
        {
            FileStream file = File.Open(path, FileMode.Create);

            file.Write(data, 0, data.Length);

            file.Flush();

            file.Close();
        }
        #endregion
    }
}
