﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Text;
using CodeJSON;

namespace CodeJSON_TestBench
{
    [TestClass]
    public class JSONTest
    {
        [TestMethod]
        public void ParseTest()
        {
            string input = "{ \"id\" : 0, \"health\" : 100, \"weapon\" : { \"id\" : 1, \"name\" : \"sword\" }, \"inventory\" : [ 12, 35, 10, { \"name\" : \"rubber_ducky\", \"damage\" : 5 }, 45], \"speed\" : 30 }";

            JSON actual = JSON.FromJSON(input);

            JSON expected = new JSON();
            expected.Get.Add("id", 0);
            expected.Get.Add("health", 100);

            JSON.Object weapon = JSON.Object.New("weapon");
            weapon.Add("id", 1);
            weapon.Add("name", "sword");
            expected.Get.AddItem(weapon);

            JSON.Array inventory = JSON.Array.New("inventory");
            inventory.Add(12, 35, 10);
            JSON.Object obj = JSON.Object.New("");
            obj.Add("name", "rubber_ducky");
            obj.Add("damage", 5);
            inventory.Add(obj);
            inventory.Add(45);
            expected.Get.AddItem(inventory);

            expected.Get.Add("speed", 30);

            Assert.AreEqual(expected, actual);
        }
    }
}
