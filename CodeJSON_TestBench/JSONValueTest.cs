using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeJSON;
using System;

namespace CodeJSON_TestBench
{
    [TestClass]
    public class JSONValueNewTest
    {
        [TestMethod]
        public void EmptyTest()
        {
            JSON.Value testedValue = JSON.Value.New();

            // Test name
            Assert.AreEqual("", testedValue.Name);

            // Test type
            Assert.AreEqual(null, testedValue.Type);

            // Test value
            Assert.AreEqual(null, testedValue.Get());
        }

        [TestMethod]
        public void BoolTest()
        {
            string name = "bool_test";
            bool value = true;
            JSON.Value testedValue = JSON.Value.New(name, value);

            // Test name
            Assert.AreEqual(name, testedValue.Name);

            // Test type
            Assert.AreEqual(typeof(bool), testedValue.Type);

            // Test value
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void IntTest()
        {
            string name = "int_test";
            int value = 42;
            JSON.Value testedValue = JSON.Value.New(name, value);

            // Test name
            Assert.AreEqual(name, testedValue.Name);

            // Test type
            Assert.AreEqual(typeof(int), testedValue.Type);

            // Test value
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void LongTest()
        {
            string name = "long_test";
            long value = 42L;
            JSON.Value testedValue = JSON.Value.New(name, value);

            // Test name
            Assert.AreEqual(name, testedValue.Name);

            // Test type
            Assert.AreEqual(typeof(long), testedValue.Type);

            // Test value
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void FloatTest()
        {
            string name = "float_test";
            float value = 0.0000054569986f;
            JSON.Value testedValue = JSON.Value.New(name, value);

            // Test name
            Assert.AreEqual(name, testedValue.Name);

            // Test type
            Assert.AreEqual(typeof(float), testedValue.Type);

            // Test value
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void DoubleTest()
        {
            string name = "double_test";
            double value = 2189653.469866531298d;
            JSON.Value testedValue = JSON.Value.New(name, value);

            // Test name
            Assert.AreEqual(name, testedValue.Name);

            // Test type
            Assert.AreEqual(typeof(double), testedValue.Type);

            // Test value
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void StringTest()
        {
            string name = "string_test";
            string value = "Hello World";
            JSON.Value testedValue = JSON.Value.New(name, value);

            // Test name
            Assert.AreEqual(name, testedValue.Name);

            // Test type
            Assert.AreEqual(typeof(string), testedValue.Type);

            // Test value
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void EmptyStringTest()
        {
            string name = "";
            string value = "";
            JSON.Value testedValue = JSON.Value.New(name, value);

            // Test name
            Assert.AreEqual(name, testedValue.Name);

            // Test type
            Assert.AreEqual(typeof(string), testedValue.Type);

            // Test value
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void NullTest()
        {
            string name = "null_test";
            JSON.Value testedValue = JSON.Value.New(name);

            // Test name
            Assert.AreEqual(name, testedValue.Name);

            // Test type
            Assert.AreEqual(null, testedValue.Type);

            // Test value
            Assert.AreEqual(null, testedValue.Get());
        }
    }

    [TestClass]
    public class JSONValueConversionTest
    {
        [TestMethod]
        public void BoolTest()
        {
            bool value = true;
            JSON.Value testedValue = JSON.Value.New("", value);
            bool actualBool = testedValue;
            int actualInt = testedValue;
            long actualLong = testedValue;
            float actualFloat = testedValue;
            double actualDouble = testedValue;
            string actualString = testedValue;

            Assert.AreEqual(value, actualBool);
            Assert.AreEqual(1, actualInt);
            Assert.AreEqual(1, actualLong);
            Assert.AreEqual(1, actualFloat);
            Assert.AreEqual(1, actualDouble);
            Assert.AreEqual(value.ToString(), actualString);
        }

        [TestMethod]
        public void IntTest()
        {
            int value = 42;
            JSON.Value testedValue = JSON.Value.New("", value);
            bool actualBool = testedValue;
            int actualInt = testedValue;
            long actualLong = testedValue;
            float actualFloat = testedValue;
            double actualDouble = testedValue;
            string actualString = testedValue;

            Assert.AreEqual(true, actualBool);
            Assert.AreEqual((int)value, actualInt);
            Assert.AreEqual((long)value, actualLong);
            Assert.AreEqual((float)value, actualFloat);
            Assert.AreEqual((double)value, actualDouble);
            Assert.AreEqual(value.ToString(), actualString);
        }

        [TestMethod]
        public void LongTest()
        {
            long value = -42L;
            JSON.Value testedValue = JSON.Value.New("", value);
            bool actualBool = testedValue;
            int actualInt = testedValue;
            long actualLong = testedValue;
            float actualFloat = testedValue;
            double actualDouble = testedValue;
            string actualString = testedValue;

            Assert.AreEqual(true, actualBool);
            Assert.AreEqual((int)value, actualInt);
            Assert.AreEqual((long)value, actualLong);
            Assert.AreEqual((float)value, actualFloat);
            Assert.AreEqual((double)value, actualDouble);
            Assert.AreEqual(value.ToString(), actualString);
        }

        [TestMethod]
        public void FloatTest()
        {
            float value = 0.00002256888f;
            JSON.Value testedValue = JSON.Value.New("", value);
            bool actualBool = testedValue;
            int actualInt = testedValue;
            long actualLong = testedValue;
            float actualFloat = testedValue;
            double actualDouble = testedValue;
            string actualString = testedValue;

            Assert.AreEqual(true, actualBool);
            Assert.AreEqual((int)value, actualInt);
            Assert.AreEqual((long)value, actualLong);
            Assert.AreEqual((float)value, actualFloat);
            Assert.AreEqual((double)value, actualDouble);
            Assert.AreEqual(value.ToString(), actualString);
        }

        [TestMethod]
        public void DoubleTest()
        {
            double value = -2369.6858985565d;
            JSON.Value testedValue = JSON.Value.New("", value);
            bool actualBool = testedValue;
            int actualInt = testedValue;
            long actualLong = testedValue;
            float actualFloat = testedValue;
            double actualDouble = testedValue;
            string actualString = testedValue;

            Assert.AreEqual(true, actualBool);
            Assert.AreEqual((int)value, actualInt);
            Assert.AreEqual((long)value, actualLong);
            Assert.AreEqual((float)value, actualFloat);
            Assert.AreEqual((double)value, actualDouble);
            Assert.AreEqual(value.ToString(), actualString);
        }

        [TestMethod]
        public void StringTest()
        {
            string value = "Test text";
            JSON.Value testedValue = JSON.Value.New("", value);
            bool actualBool = testedValue;
            int actualInt = testedValue;
            long actualLong = testedValue;
            float actualFloat = testedValue;
            double actualDouble = testedValue;
            string actualString = testedValue;

            Assert.AreEqual(true, actualBool);
            Assert.AreEqual(0, actualInt);
            Assert.AreEqual(0, actualLong);
            Assert.AreEqual(0, actualFloat);
            Assert.AreEqual(0, actualDouble);
            Assert.AreEqual(value.ToString(), actualString);
        }

        [TestMethod]
        public void NullTest()
        {
            JSON.Value testedValue = JSON.Value.New("");
            bool actualBool = testedValue;
            int actualInt = testedValue;
            long actualLong = testedValue;
            float actualFloat = testedValue;
            double actualDouble = testedValue;
            string actualString = testedValue;

            Assert.AreEqual(false, actualBool);
            Assert.AreEqual(0, actualInt);
            Assert.AreEqual(0, actualLong);
            Assert.AreEqual(0, actualFloat);
            Assert.AreEqual(0, actualDouble);
            Assert.AreEqual("", actualString);
        }

    }

    [TestClass]
    public class JSONValueSetTest
    {
        [TestMethod]
        public void BoolTest()
        {
            JSON.Value testedValue = JSON.Value.New("");

            bool value = true;
            testedValue.Set(value);

            // Test type
            Assert.AreEqual(typeof(bool), testedValue.Type);

            // Test value
            Assert.IsInstanceOfType(testedValue.Get(), typeof(bool));
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void IntTest()
        {
            JSON.Value testedValue = JSON.Value.New("");

            int value = 42;
            testedValue.Set(value);

            // Test type
            Assert.AreEqual(typeof(int), testedValue.Type);

            // Test value
            Assert.IsInstanceOfType(testedValue.Get(), typeof(int));
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void LongTest()
        {
            JSON.Value testedValue = JSON.Value.New("");

            long value = -42L;
            testedValue.Set(value);

            // Test type
            Assert.AreEqual(typeof(long), testedValue.Type);

            // Test value
            Assert.IsInstanceOfType(testedValue.Get(), typeof(long));
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void FloatTest()
        {
            JSON.Value testedValue = JSON.Value.New("");

            float value = 3.1415926535897932f;
            testedValue.Set(value);

            // Test type
            Assert.AreEqual(typeof(float), testedValue.Type);

            // Test value
            Assert.IsInstanceOfType(testedValue.Get(), typeof(float));
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void DoubleTest()
        {
            JSON.Value testedValue = JSON.Value.New("");

            double value = Math.PI;
            testedValue.Set(value);

            // Test type
            Assert.AreEqual(typeof(double), testedValue.Type);

            // Test value
            Assert.IsInstanceOfType(testedValue.Get(), typeof(double));
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void StringTest()
        {
            JSON.Value testedValue = JSON.Value.New("");

            string value = "Hello World";
            testedValue.Set(value);

            // Test type
            Assert.AreEqual(typeof(string), testedValue.Type);

            // Test value
            Assert.IsInstanceOfType(testedValue.Get(), typeof(string));
            Assert.AreEqual(value, testedValue.Get());
        }

        [TestMethod]
        public void NullTest()
        {
            JSON.Value testedValue = JSON.Value.New("");

            testedValue.Set();

            // Test type
            Assert.AreEqual(null, testedValue.Type);

            // Test value
            Assert.AreEqual(null, testedValue.Get());
        }
    }

    [TestClass]
    public class JSONValueEqualsTest
    {
        [TestMethod]
        public void BoolTest()
        {
            JSON.Value testedValue0 = JSON.Value.New("name", true);
            JSON.Value testedValue1 = JSON.Value.New("name", true);
            JSON.Value testedValue2 = JSON.Value.New("name", false);

            Assert.IsTrue(testedValue0.Equals(testedValue1));
            Assert.IsFalse(testedValue0.Equals(testedValue2));
        }

        [TestMethod]
        public void IntTest()
        {
            JSON.Value testedValue0 = JSON.Value.New("name", 42);
            JSON.Value testedValue1 = JSON.Value.New("name", 42L);
            JSON.Value testedValue2 = JSON.Value.New("name", 42);

            Assert.IsFalse(testedValue0.Equals(testedValue1));
            Assert.IsTrue(testedValue0.Equals(testedValue2));
        }

        [TestMethod]
        public void LongTest()
        {
            JSON.Value testedValue0 = JSON.Value.New("name", 42L);
            JSON.Value testedValue1 = JSON.Value.New("name", 42);
            JSON.Value testedValue2 = JSON.Value.New("name", 42L);

            Assert.IsFalse(testedValue0.Equals(testedValue1));
            Assert.IsTrue(testedValue0.Equals(testedValue2));
        }

        [TestMethod]
        public void FloatTest()
        {
            JSON.Value testedValue0 = JSON.Value.New("name", 42.0f);
            JSON.Value testedValue1 = JSON.Value.New("name", 42.0d);
            JSON.Value testedValue2 = JSON.Value.New("name", 42.0f);

            Assert.IsFalse(testedValue0.Equals(testedValue1));
            Assert.IsTrue(testedValue0.Equals(testedValue2));
        }

        [TestMethod]
        public void DoubleTest()
        {
            JSON.Value testedValue0 = JSON.Value.New("name", 42.0d);
            JSON.Value testedValue1 = JSON.Value.New("name", 42.0f);
            JSON.Value testedValue2 = JSON.Value.New("name", 42.0d);

            Assert.IsFalse(testedValue0.Equals(testedValue1));
            Assert.IsTrue(testedValue0.Equals(testedValue2));
        }

        [TestMethod]
        public void StringTest()
        {
            JSON.Value testedValue0 = JSON.Value.New("name", "42");
            JSON.Value testedValue1 = JSON.Value.New("name", 42);
            JSON.Value testedValue2 = JSON.Value.New("name", "42");

            Assert.IsFalse(testedValue0.Equals(testedValue1));
            Assert.IsTrue(testedValue0.Equals(testedValue2));
        }

        [TestMethod]
        public void NullTest()
        {
            JSON.Value testedValue0 = JSON.Value.New("name");
            JSON.Value testedValue1 = JSON.Value.New("name", 42);
            JSON.Value testedValue2 = JSON.Value.New("name");

            Assert.IsFalse(testedValue0.Equals(testedValue1));
            Assert.IsTrue(testedValue0.Equals(testedValue2));
        }
    }

    [TestClass]
    public class JSONValueSerializationTest
    {
        [TestMethod]
        public void BoolToString()
        {
            JSON.Value original = JSON.Value.New("bool", true);

            // Test ToString()
            string expected = "\"bool\" : true";
            string actual = original.ToString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IntToString()
        {
            JSON.Value original = JSON.Value.New("int", 42);

            // Test ToString()
            string expected = "\"int\" : 42";
            string actual = original.ToString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void LongToString()
        {
            JSON.Value original = JSON.Value.New("long", 42L);

            // Test ToString()
            string expected = "\"long\" : 42";
            string actual = original.ToString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FloatToString()
        {
            JSON.Value original = JSON.Value.New("float", 0.000000042f);

            // Test ToString()
            string expected = "\"float\" : 4.2E-08";
            string actual = original.ToString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DoubleToString()
        {
            JSON.Value original = JSON.Value.New("double", 42.4242424242424242424242d);

            // Test ToString()
            string expected = "\"double\" : 42.42424242424242";
            string actual = original.ToString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void StringToString()
        {
            JSON.Value original = JSON.Value.New("string", "42");

            // Test ToString()
            string expected = "\"string\" : \"42\"";
            string actual = original.ToString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void NullToString()
        {
            JSON.Value original = JSON.Value.New("null");

            // Test ToString()
            string expected = string.Concat("\"null\" : null");
            string actual = original.ToString();
            Assert.AreEqual(expected, actual);
        }
    }
}
