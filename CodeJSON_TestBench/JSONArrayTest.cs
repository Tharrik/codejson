﻿using CodeJSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeJSON_TestBench
{
    [TestClass]
    public class JSONArrayRemoveTest
    {
        [TestMethod]
        public void BoolTest()
        {
            JSON.Array actual = JSON.Array.New("name", true, true, false);
            actual.Remove(true);

            JSON.Array expected = JSON.Array.New("name", true, false);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IntTest()
        {
            JSON.Array actual = JSON.Array.New("name", 0, 1, 2);
            actual.Remove(1);

            JSON.Array expected = JSON.Array.New("name", 0, 2);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void LongTest()
        {
            JSON.Array actual = JSON.Array.New("name", 0L, 1L, 2L);
            actual.Remove(1L);

            JSON.Array expected = JSON.Array.New("name", 0L, 2L);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FloatTest()
        {
            JSON.Array actual = JSON.Array.New("name", 1.62f, 2.72f, 3.14f);
            actual.Remove(2.72f);

            JSON.Array expected = JSON.Array.New("name", 1.62f, 3.14f);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DoubleTest()
        {
            JSON.Array actual = JSON.Array.New("name", 1.6180339887499d, 2.718281828459d, 3.14159265358979d);
            actual.Remove(2.718281828459d);

            JSON.Array expected = JSON.Array.New("name", 1.6180339887499d, 3.14159265358979d);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void StringTest()
        {
            JSON.Array actual = JSON.Array.New("name", "I", "was", "never", "here");
            actual.Remove("never");

            JSON.Array expected = JSON.Array.New("name", "I", "was", "here");

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void NullTest()
        {
            JSON.Array actual = JSON.Array.New("name", 0, 1, 2);
            actual.Add();
            actual.Add();
            actual.Add(3);
            actual.Remove();

            JSON.Array expected = JSON.Array.New("name", 0, 1, 2);
            expected.Add();
            expected.Add(3);

            Assert.AreEqual(expected, actual);
        }
    }

    [TestClass]
    public class JSONArrayRemoveAllTest
    {
        [TestMethod]
        public void BoolTest()
        {
            JSON.Array actual = JSON.Array.New("", true, true, false, true, false);
            actual.RemoveAll(false);

            JSON.Array expected = JSON.Array.New("", true, true, true);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IntTest()
        {
            JSON.Array actual = JSON.Array.New("", 0, 1, 2, 1, 1, 3, 4);
            actual.RemoveAll(1);

            JSON.Array expected = JSON.Array.New("", 0, 2, 3, 4);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void LongTest()
        {
            JSON.Array actual = JSON.Array.New("", 0L, 1L, 2L, 1L, 1L, 3L, 4L);
            actual.RemoveAll(1L);

            JSON.Array expected = JSON.Array.New("", 0L, 2L, 3L, 4L);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FloatTest()
        {
            JSON.Array actual = JSON.Array.New("", 0.1f, 1.2f, 0.1f, 0.8f, 0.7f);
            actual.RemoveAll(0.1f);

            JSON.Array expected = JSON.Array.New("", 1.2f, 0.8f, 0.7f);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DoubleTest()
        {
            JSON.Array actual = JSON.Array.New("", 2.3d, 5.2d, 3.2d, 3.2d, 5.1d);
            actual.RemoveAll(3.2d);

            JSON.Array expected = JSON.Array.New("", 2.3d, 5.2d, 5.1d);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void StringTest()
        {
            JSON.Array actual = JSON.Array.New("", "A", "B", "C", "S", "A", "A");
            actual.RemoveAll("A");

            JSON.Array expected = JSON.Array.New("", "B", "C", "S");

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void NullTest()
        {
            JSON.Array actual = JSON.Array.New("", 0, 1, 2);
            actual.Add();
            actual.Add("Hello");
            actual.Add();
            actual.Add();
            actual.RemoveAll();

            JSON.Array expected = JSON.Array.New("", 0, 1, 2);
            expected.Add("Hello");

            Assert.AreEqual(expected, actual);
        }


    }

    [TestClass]
    public class JSONArrayEqualsTest
    {
        [TestMethod]
        public void IntTest()
        {
            JSON.Array array0 = JSON.Array.New("", 0, 1, 2, 13);
            JSON.Array array1 = JSON.Array.New("", 0, 1, 2, 13);
            JSON.Array array2 = JSON.Array.New("", 0, 1, 2);

            Assert.IsTrue(array0.Equals(array1));
            Assert.IsFalse(array0.Equals(array2));
        }
    }

    [TestClass]
    public class JSONArrayToStringTest
    {
        [TestMethod]
        public void IntTest()
        {
            JSON.Array array = JSON.Array.New("array", 5, 7, 9, 3, 6);
            string actual = array.ToString();

            string expected = "\"array\" : [5, 7, 9, 3, 6]";

            Assert.AreEqual(expected, actual);
        }
    }
}
