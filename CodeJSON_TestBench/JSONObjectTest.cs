﻿using CodeJSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeJSON_TestBench
{
    [TestClass]
    public class JSONObjectTest
    {
        [TestMethod]
        public void EqualsTest()
        {
            JSON.Object obj0 = JSON.Object.New("name");
            JSON.Object obj1 = JSON.Object.New("name");
            JSON.Object obj2 = JSON.Object.New("gnome");

            Assert.IsTrue(JSON.Object.Equals(obj0, obj1));
            Assert.IsFalse(JSON.Object.Equals(obj0, obj2));
        }

        [TestMethod]
        public void ToStringTest()
        {
            JSON.Object obj = JSON.Object.New("name");

            obj.Add("number", 42);
            obj.Add("health", 100);
            obj.Add("isalive", true);
            obj.Add("pi", 3.14f);
            obj.AddItem(JSON.Array.New("inventory", "sword", "shield", "rubberChicken"));

            string actual = obj.ToString();
            string expected = "\"name\" : { \"number\" : 42, \"health\" : 100, \"isalive\" : true, \"pi\" : 3.14, \"inventory\" : [\"sword\", \"shield\", \"rubberChicken\"]}";

            Assert.AreEqual(expected, actual);
        }
    }
}
