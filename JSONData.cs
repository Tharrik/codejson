﻿using System;

namespace CodeJSON
{
    public partial class JSON
    {
        public abstract class Data
        {
            public string Name { get; internal set; }
            public Type Type { get; internal set; }

            internal Data(string name, Type type)
            {
                Name = name;
                Type = type;
            }

            public override bool Equals(object obj)
            {
                if (obj == null || !GetType().Equals(obj.GetType())) return false;

                JSON.Data other = obj as JSON.Data;

                return
                    System.Type.Equals(Type, other.Type) &&
                    string.Equals(Name, other.Name);
            }

            #region Internal methods
            protected static bool IsWhitespace(char c)
            {
                return c == ' ' || c == '\n' || c == '\r' || c == '\t';
            }
            #endregion

            #region Serialization
            protected static string ExtractString(string s, ref int index)
            {
                string toReturn = "";
                int size = s.Length;

                for (++index; index < size && s[index] != '\"'; ++index)
                {
                    toReturn += s[index];
                }

                return toReturn;
            }

            public override string ToString()
            {
                return string.Concat("\"", Name, "\" : ");
            }

            internal abstract string ValueToString();
            #endregion
        }
    }
}
