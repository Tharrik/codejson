﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace CodeJSON
{
    public partial class JSON
    {

        public class Value : Data
        {
            #region Static Creators
            public static Value New()
            {
                return new Value("", null, null);
            }

            public static Value New(string name)
            {
                return new Value(name, null, null);
            }

            public static Value New(string name, bool value)
            {
                return new Value(name, typeof(bool), BitConverter.GetBytes(value));
            }

            public static Value New(string name, int value)
            {
                return new Value(name, typeof(int), BitConverter.GetBytes(value));
            }

            public static Value New(string name, long value)
            {
                return new Value(name, typeof(long), BitConverter.GetBytes(value));
            }

            public static Value New(string name, float value)
            {
                return new Value(name, typeof(float), BitConverter.GetBytes(value));
            }

            public static Value New(string name, double value)
            {
                return new Value(name, typeof(double), BitConverter.GetBytes(value));
            }

            public static Value New(string name, string value)
            {
                return new Value(name, typeof(string), Encoding.Unicode.GetBytes(value));
            }
            #endregion

            internal const int floatPrecision = 8;

            #region Fields
            private byte[] value;
            #endregion

            #region Constructor
            internal Value(string name, Type type, byte[] value) : base(name, type)
            {
                this.value = value;
            }
            #endregion

            #region Conversion operators
            public static implicit operator bool(Value value)
            {
                bool toReturn = false;

                if (value.Type == typeof(bool)) toReturn = value.Get();
                else if (value.Type == typeof(int)) toReturn = value.Get() != 0;
                else if (value.Type == typeof(long)) toReturn = value.Get() != 0;
                else if (value.Type == typeof(float)) toReturn = value.Get() != 0;
                else if (value.Type == typeof(double)) toReturn = value.Get() != 0;
                else if (value.Type == typeof(string)) toReturn = value.Get() != "0" && value.Get().ToLower() != "false";

                return toReturn;
            }

            public static implicit operator int(Value value)
            {
                int toReturn = 0;

                if (value.Type == typeof(bool)) toReturn = value.Get() ? 1 : 0;
                else if (value.Type == typeof(int)) toReturn = value.Get();
                else if (value.Type == typeof(long)) toReturn = (int)value.Get();
                else if (value.Type == typeof(float)) toReturn = (int)value.Get();
                else if (value.Type == typeof(double)) toReturn = (int)value.Get();
                else if (value.Type == typeof(string) && int.TryParse(value.Get(), out toReturn)) { }

                return toReturn;
            }

            public static implicit operator long(Value value)
            {
                long toReturn = 0;

                if (value.Type == typeof(bool)) toReturn = value.Get() ? 1 : 0;
                else if (value.Type == typeof(int)) toReturn = (long)value.Get();
                else if (value.Type == typeof(long)) toReturn = value.Get();
                else if (value.Type == typeof(float)) toReturn = (long)value.Get();
                else if (value.Type == typeof(double)) toReturn = (long)value.Get();
                else if (value.Type == typeof(string) && long.TryParse(value.Get(), out toReturn)) { }

                return toReturn;
            }

            public static implicit operator float(Value value)
            {
                float toReturn = 0.0f;

                if (value.Type == typeof(bool)) toReturn = value.Get() ? 1.0f : 0.0f;
                else if (value.Type == typeof(int)) toReturn = (float)value.Get();
                else if (value.Type == typeof(long)) toReturn = (float)value.Get();
                else if (value.Type == typeof(float)) toReturn = value.Get();
                else if (value.Type == typeof(double)) toReturn = (float)value.Get();
                else if (value.Type == typeof(string) && float.TryParse(value.Get(), out toReturn)) { }

                return toReturn;
            }

            public static implicit operator double(Value value)
            {
                double toReturn = 0.0d;

                if (value.Type == typeof(bool)) toReturn = value.Get() ? 1.0d : 0.0d;
                else if (value.Type == typeof(int)) toReturn = (double)value.Get();
                else if (value.Type == typeof(long)) toReturn = (double)value.Get();
                else if (value.Type == typeof(float)) toReturn = (double)value.Get();
                else if (value.Type == typeof(double)) toReturn = value.Get();
                else if (value.Type == typeof(string) && double.TryParse(value.Get(), out toReturn)) { }

                return toReturn;
            }

            public static implicit operator string(Value value)
            {
                string toReturn = "";

                if (value.Type == typeof(bool)) toReturn = value.Get() ? "True" : "False";
                else if (value.Type == typeof(int)) toReturn = ((int)value.Get()).ToString();
                else if (value.Type == typeof(long)) toReturn = ((long)value.Get()).ToString();
                else if (value.Type == typeof(float)) toReturn = ((float)value.Get()).ToString();
                else if (value.Type == typeof(double)) toReturn = ((double)value.Get()).ToString();
                else if (value.Type == typeof(string)) toReturn = value.Get();

                return toReturn;
            }
            #endregion

            #region Setters
            public void Set()
            {
                this.value = null;
                Type = null;
            }

            public void Set(bool value)
            {
                this.value = BitConverter.GetBytes(value);
                Type = typeof(bool);
            }

            public void Set(int value)
            {
                this.value = BitConverter.GetBytes(value);
                Type = typeof(int);
            }

            public void Set(long value)
            {
                this.value = BitConverter.GetBytes(value);
                Type = typeof(long);
            }

            public void Set(float value)
            {
                this.value = BitConverter.GetBytes(value);
                Type = typeof(float);
            }

            public void Set(double value)
            {
                this.value = BitConverter.GetBytes(value);
                Type = typeof(double);
            }

            public void Set(string value)
            {
                this.value = Encoding.Unicode.GetBytes(value);
                Type = typeof(string);
            }
            #endregion

            #region Getters
            public dynamic Get()
            {
                if (Type == typeof(bool)) { return BitConverter.ToBoolean(value); }
                if (Type == typeof(int)) { return BitConverter.ToInt32(value); }
                if (Type == typeof(long)) { return BitConverter.ToInt64(value); }
                if (Type == typeof(float)) { return BitConverter.ToSingle(value); }
                if (Type == typeof(double)) { return BitConverter.ToDouble(value); }
                if (Type == typeof(string)) { return Encoding.Unicode.GetString(value); }

                return null;
            }
            #endregion

            public override bool Equals(object obj)
            {
                JSON.Value value = obj as JSON.Value;

                return base.Equals(obj) && Equals(Get(), value.Get());
            }

            #region Serialization
            internal static JSON.Value Parse(string input, ref int index)
            {
                JSON.Value toReturn = JSON.Value.New("");

                if (input[index] == '"') toReturn.Set(ExtractString(input, ref index));
                else
                {
                    string valueString = "";
                    int size = input.Length;
                    bool pending = true;

                    for(; pending && index < size; ++index)
                    {
                        char current = input[index];

                        if(current != ',' && current != ' ' && current != ']' && current != '}')
                        {
                            valueString += current;
                        }
                        else
                        {
                            if (valueString == "null") toReturn.Set();
                            else if (bool.TryParse(valueString, out bool valueB)) toReturn.Set(valueB);
                            else if (int.TryParse(valueString, out int valueI)) toReturn.Set(valueI);
                            else if (long.TryParse(valueString, out long valueL)) toReturn.Set(valueL);
                            else if (valueString.Length <= floatPrecision &&
                                float.TryParse(valueString, NumberStyles.Any, CultureInfo.InvariantCulture, out float valueF)) toReturn.Set(valueF);
                            else if (double.TryParse(valueString, NumberStyles.Any, CultureInfo.InvariantCulture, out double valueD)) toReturn.Set(valueD);

                            pending = false;
                        }
                    }

                    index -= 2;
                }

                return toReturn;
            }

            public override string ToString()
            {
                return string.Concat('\"', Name, "\" : ", ValueToString());
            }

            internal override string ValueToString()
            {
                if (Type == null) return "null";
                if (Type == typeof(string)) return string.Concat("\"", Get(), "\"");
                if (Type == typeof(bool)) return Get() ? "true" : "false";
                if (Type == typeof(float) || Type == typeof(double))
                {
                    return Get().ToString(CultureInfo.InvariantCulture);
                }
                return Get().ToString();
            }
            #endregion
        }

    }
}
